<div class="container-fluid p-0">

	<div id="contact-us" style="padding-top: 135px;" class="clearfix contact">
        <!-- <div class="bg-white" data-aos="fade-up" style="height: 450px;">
        	<div class="container">
				<div class="row">
					<div class="col-lg-12 mt-5">
						<h2 class="text-dark text-center">
							<?= $title ?>
						</h2>
					</div>
				</div>
			</div>
		</div> -->
		<div class="container" data-aos="fade-up">

			<div class="section-title">
				<h3><span>Contact Us</span></h3>
			</div>

			<div class="row" data-aos="fade-up" data-aos-delay="100">
				<div class="col-lg-6">
					<div class="info-box mb-4 bg-white">
						<i class="bx bx-map"></i>
						<h3>Our Address</h3>
						<p>Jl. Jupiter Sel. No.2, Sekejati, Kec. Buahbatu, Kota Bandung, Jawa Barat 40286</p>
					</div>
				</div>

				<div class="col-lg-3 col-md-6">
					<div class="info-box mb-4 bg-white">
						<i class="bx bx-envelope"></i>
						<h3>Email Us</h3>
						<p>contact@medianet.co.id</p>
					</div>
				</div>

				<div class="col-lg-3 col-md-6">
					<div class="info-box mb-4 bg-white">
						<i class="bx bx-phone-call"></i>
						<h3>Call Us</h3>
						<p>(022) 87528027</p>
					</div>
				</div>

			</div>

			<div class="row" data-aos="fade-up" data-aos-delay="100">

				<div class="col-lg-6 bg-white">
					<form action="<?= base_url("contactus/save_contact") ?>" method="post" role="form" class="php-email-form">
						<div class="row">
							<div class="col form-group">
								<input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
								<div class="validate"></div>
							</div>
							<div class="col form-group">
								<input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
								<div class="validate"></div>
							</div>
						</div>
						<div class="form-group">
							<input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
							<div class="validate"></div>
						</div>
						<div class="form-group">
							<textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
							<div class="validate"></div>
						</div>
						<div class="mb-3">
							<div class="loading">Loading</div>
							<div class="error-message"></div>
							<div class="sent-message">Your message has been sent. Thank you!</div>
						</div>
						<div class="text-left"><button type="submit">Send Message</button></div>
					</form>
				</div>


				<div class="col-lg-6 ">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.5171092310693!2d107.65882681438725!3d-6.948163069950882!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e821cc787f21%3A0x610e2c57916c543!2sMedianet%20Technology.%20PT!5e0!3m2!1sid!2sid!4v1620181904431!5m2!1sid!2sid" style="border:0; width: 100%; height: 384px;" allowfullscreen="" loading="lazy"></iframe>
				</div>

			</div>

		</div>
	</div>

</div>