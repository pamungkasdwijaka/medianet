<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Contactus extends Mnet_Controller {

	
	function __construct()
	{
		parent::__construct();
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Contactus';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			// 'assets/js/app/home.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
		);

		$this->template->load($this->data, null, 'index');

	}

	public function save_contact_post()
	{
		$data = [
			"name"		=> $this->input->post("name"),
			"email"		=> $this->input->post("email"),
			"subject"	=> $this->input->post("subject"),
			"message"	=> $this->input->post("message"),
			"created_at"=> date("Y-m-d H:i:s"),
		];

		$sql = $this->db->insert("contact", $data);

		if ($sql == 1) {
			$return = [
				"msg" => "OK"
			];
			$this->response($return);
		}
	}

}
