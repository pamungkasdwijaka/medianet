<div ng-controller="signup">
	<div id="sign">
		<div class="container" style="height: 300px;">
			<div class="row">
				<div class="col-lg-12 pt-5 pt-lg-0 order-2 order-lg-1 mt-5 text-center">
					<div data-aos="zoom-out">
						<!-- <h3 class="text-white"><b>Member Sign Up Application</b></h3> -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		fbq('track', 'Lead');
	</script>

	<!-- Thank You -->
	<div class="signup">
		<div class="container">

			<div class="row">

				<div class="col-lg-12 bg-thx p-5 text-center" data-aos="fade-left">
					<p style="text-align: center;">
						<strong>THANK YOU <br />
							The signup form was submitted successfully ! <br /> 
							We will get back to you after reviewing your application. <br />
							Please check your email inbox to sign the agreement. <br /> <br />
							If there's a problem in receiving the email, please contact: <a target="_blank"
                                href="mailto:support@neutrafix.freshdesk.com?subject=" class="text-danger">support@neutrafix.freshdesk.com</a> <br />
						</strong>
					</p>
				</div>

			</div>

		</div>
	</div>
	<!-- End #main -->
</div>
