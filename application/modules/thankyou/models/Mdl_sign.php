<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_sign extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
        $query = $this->db->insert("signup", $data);

        return $query;
    }
}
