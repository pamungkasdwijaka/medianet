<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Thankyou extends Neu_Controller {

	
	function __construct()
	{
		parent::__construct();
		// $this->load->model(["mdl_home"]);
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Thank You';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			// 'assets/js/app/signup.js?'.date("Ymd His")
		);

		$this->data['css'] = array(
			'assets/css/app/signup.css?'.date("Ymd His")
		);

		$this->template->load($this->data, null, 'index');

	}

	public function forcontact_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Thank You';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			// 'assets/js/app/signup.js?'.date("Ymd His")
		);

		$this->data['css'] = array(
			'assets/css/app/signup.css?'.date("Ymd His")
		);

		$this->template->load($this->data, null, 'thx');

	}

	public function thanks_for_contact_get()
	{
		// Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Thank You';
		$this->data['version'] = $this->uri->segment(1);

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			// 'assets/js/app/signup.js?'.date("Ymd His")
		);

		$this->data['css'] = array(
			'assets/css/app/signup.css?'.date("Ymd His")
		);

		$this->template->load($this->data, null, 'thankshubspot');
	}
}
