<div>
    
    <section id="intro" class="clearfix">
        <div class="container-fluid p-0" data-aos="fade-up">

            <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                        <div class="carousel-item active" data-interval="10000">
                            <div class="intro-info" data-aos="zoom-in" data-aos-delay="100">
                                <h2 class="text-white">Deliver New Services</h2>
                                <small class="text-white clearfix">Anywhere, Anytime, Any Network</small>
                                <img src="assets/img/laptop-pc.png" class="mt-5 img-fluid img-mdnet">
                            </div>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <div class="intro-info" data-aos="zoom-in" data-aos-delay="100">
                                <h2 class="text-white">Fast Result and High Quality</h2>
                                <small class="text-white clearfix">Anywhere, Anytime, Any Network</small>
                                <small class="text-white clearfix"></small>
                                <img src="assets/img/high-quality.png" class="mt-5 img-fluid img-mdnet">
                            </div>
                        </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                  </a>
            </div>

        </div>
    </section>

    <!-- ======= About Section ======= -->
    <section id="about" class="mt-2">
        <div class="about-container container">
            <div class="row bg-white">
                <div class="col-lg-6 p-5" data-aos="fade-right">
                    <h5>ABOUT PT. MEDIANET TECHNOLOGY</h5>
                    <p class="mb-3" style="font-size: 12px;">
                        Medianet Technology is company a with a wide range of products, technologies,
                        services, and partnerships for business computing in Indonesia. Medianet has a
                        long-term vision for what we can do for you together. Throught our e-Business Solutions
                        services, Medianet can serve as an ideal long-term partner to help you apply information
                        Technology to your business. Ensuring quality solutions for our customers,
                        Medianet choose only quality brands backed up by excellent business partners
                        internationally acknowledge companies with the right connections, experience and proven
                        tracks records. Medianet has developed partnerships and alliances to maximize its own
                        strength in developing quality IT solutions that will provide many advantages for you
                        business today and enable expansion in the future.
                    </p>
                </div>
                <div class="col-lg-6"  data-aos="fade-left">
                    <img style="margin-left: 12px;" src="assets/img/about/rawpixel-579231-unsplash.jpg" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <!-- End About Section -->


    <!-- ======= General Service ======= -->
    <section id="general-service" class="mt-0 bg-general-service mb-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-3">
                    <h3 class="text-center">Our Client</h3>
                </div>
                <div class="col-lg-4 mt-3" data-aos="fade-left">
                    <div class="card p-3 card-medianet">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <i class="icofont-book icon-education"></i>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <h5 class="mt-3">
                                        Educational Organisations
                                    </h5>
                                </div>
                            </div>
                            <div class="card-body-medianet p-4 text-center">
                                <img class="img-fluid" width="150" height="150" src="https://klis.sch.id/wp-content/uploads/2020/08/sticker-mobil-no-pond-300x105.png">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mt-3" data-aos="fade-up">
                    <div class="card p-3 card-medianet">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <i class="icofont-briefcase icon-education"></i>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <h5 class="mt-3">
                                        Enterprise Organisations
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body-medianet p-4 text-center">
                            <img class="img-fluid" width="150" height="150" src="https://www.telkom.co.id/images/logo_horizontal.svg">         
                            <img class="img-fluid" width="150" height="150" src="<?= base_url("assets/img/client/logo-telkomsel.png") ?>">
                            <img class="img-fluid mt-2" width="100" height="100" src="<?= base_url("assets/img/client/telin.png") ?>">
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mt-3">
                    <div class="card p-3 card-medianet" data-aos="fade-right">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <i class="icofont-vector-path icon-education"></i>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <h5 class="mt-3">
                                        Internet based companies
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body-medianet p-4 text-center">
                            <img class="img-fluid" width="150" height="150" src="https://www.telkom.co.id/images/logo_horizontal.svg">         
                            <img class="img-fluid" width="150" height="150" src="<?= base_url("assets/img/client/logo-telkomsel.png") ?>">
                            <img class="img-fluid mt-2" width="100" height="100" src="<?= base_url("assets/img/client/telin.png") ?>">   
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mt-3">
                    <div class="card p-3 card-medianet" data-aos="fade-left">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <i class="icofont-handshake-deal icon-education"></i>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <h5 class="mt-3">
                                        Business Services
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body-medianet p-4 text-center">
                            <img class="img-fluid" width="150" height="150" src="https://www.telkom.co.id/images/logo_horizontal.svg">         
                            <img class="img-fluid" width="150" height="150" src="<?= base_url("assets/img/client/logo-telkomsel.png") ?>">
                            <img class="img-fluid mt-2" width="100" height="100" src="<?= base_url("assets/img/client/telin.png") ?>">
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mt-3">
                    <div class="card p-3 card-medianet" data-aos="fade-up">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <i class="icofont-institution icon-education"></i>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <h5 class="mt-3">
                                        Telco Organizations
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body-medianet p-4 text-center">
                            <img class="img-fluid" width="150" height="150" src="https://www.telkom.co.id/images/logo_horizontal.svg">         
                            <img class="img-fluid" width="150" height="150" src="<?= base_url("assets/img/client/logo-telkomsel.png") ?>">
                            <img class="img-fluid mt-2" width="100" height="100" src="<?= base_url("assets/img/client/telin.png") ?>">
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mt-3">
                    <div class="card p-3 card-medianet" data-aos="fade-right">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <i class="icofont-tools-alt-2 icon-education"></i>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <h5 class="mt-3">
                                        Engineering Companies
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body-medianet p-4 text-center">
                            <img class="img-fluid" width="90" height="90" src="<?= base_url("assets/img/client/bri.png") ?>">
                        </div>
                    </div>
                </div>

                <!-- <div class="col-lg-4 mt-3">
                    <div class="card p-3 card-medianet" data-aos="fade-left">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <i class="icofont-industries-4 icon-education"></i>
                                </div>
                                <div class="col-lg-12 text-center">
                                    <h5 class="mt-3">
                                        Manufacturing Companies
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <div class="card-body-medianet p-4">
                            <img class="img-fluid" src="https://www.telkom.co.id/images/logo_horizontal.svg">                                
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!-- End General Service -->

    <!-- ======= Services Section ======= -->
    <section id="services-our" class="mt-0 services-our bg-medianet">
        <div class="container">
            <!-- <div class="row">
                <div class="col-lg-12"> -->
                    <h3 class=" text-center text-white">Our Services</h3>
                    <div class="row mt-5">
                        <div data-aos="fade-left" class="col-md-6 mt-3 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <h4 class="title text-left"><a href="">Consultation</a></h4>
                                <p class="description">
                                    Medianet Technology have dedicated team
                                    with considerable experience of business 
                                    and technical operations in transaction 
                                    service,non profit organization, public sector
                                    and commercial organisations.
                                </p>
                                <a href="<?php echo base_url("services/detail/consultation") ?>" class="btn btn-danger btn-sm mt-2" style="position: relative !important; z-index: 1000;">Read More</a>
                                <i class="bg-icon icofont-education text-mdnet text-right"></i>
                            </div>
                        </div>
                        <div data-aos="fade-up" class="col-md-6 mt-3 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <h4 class="title text-left"><a href="">Development</a></h4>
                                <p class="description">
                                    Medianet Technology have dedicated team
                                    with considerable experience of business 
                                    and technical operations in transaction 
                                    service,non profit organization, public sector
                                    and commercial organisations.
                                </p>
                                <a href="" class="btn btn-danger btn-sm mt-2" style="position: relative !important; z-index: 1000;">Read More</a>
                                <i class="icofont-dashboard-web bg-icon text-mdnet text-right"></i>
                            </div>
                        </div>
                        <div data-aos="fade-right" class="col-md-6 mt-3 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <h4 class="title text-left"><a href="">Integration</a></h4>
                                <p class="description">
                                    Medianet Technology have dedicated team
                                    with considerable experience of business 
                                    and technical operations in transaction 
                                    service,non profit organization, public sector
                                    and commercial organisations.
                                </p>
                                <a href="" class="btn btn-danger btn-sm mt-2" style="position: relative !important; z-index: 1000;">Read More</a>
                                <i class="icofont-server bg-icon text-mdnet text-right"></i>
                            </div>
                        </div>
                        <div data-aos="fade-left" class="col-md-6 mt-3 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <h4 class="title text-left"><a href="">Implementation</a></h4>
                                <p class="description">
                                    Medianet Technology have dedicated team
                                    with considerable experience of business 
                                    and technical operations in transaction 
                                    service,non profit organization, public sector
                                    and commercial organisations.
                                </p>
                                <a href="" class="btn btn-danger btn-sm mt-2" style="position: relative !important; z-index: 1000;">Read More</a>
                                <i class="icofont-terminal bg-icon text-mdnet text-right"></i>
                            </div>
                        </div>
                        <div data-aos="fade-up" class="col-md-6 mt-3 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <h4 class="title text-left"><a href="">Maintenance</a></h4>
                                <p class="description">
                                    Medianet Technology have dedicated team
                                    with considerable experience of business 
                                    and technical operations in transaction 
                                    service,non profit organization, public sector
                                    and commercial organisations.
                                </p>
                                <a href="" class="btn btn-danger btn-sm mt-2" style="position: relative !important; z-index: 1000;">Read More</a>
                                <i class="icofont-tools-alt-2 bg-icon text-mdnet text-right"></i>
                            </div>
                        </div>
                        <div data-aos="fade-right" class="col-md-6 mt-3 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <h4 class="title text-left"><a href="">Mediation Device Billing</a></h4>
                                <p class="description">
                                    Medianet Technology have dedicated team
                                    with considerable experience of business 
                                    and technical operations in transaction 
                                    service,non profit organization, public sector
                                    and commercial organisations.
                                </p>
                                <a href="" class="btn btn-danger btn-sm mt-2" style="position: relative !important; z-index: 1000;">Read More</a>
                                <i class="icofont-ui-copy bg-icon text-mdnet text-right"></i>
                            </div>
                        </div>
                        <div data-aos="fade-left"class="col-md-6 mt-3 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <h4 class="title text-left"><a href="">Automatic Network Report</a></h4>
                                <p class="description">
                                    Medianet Technology have dedicated team
                                    with considerable experience of business 
                                    and technical operations in transaction 
                                    service,non profit organization, public sector
                                    and commercial organisations.
                                </p>
                                <a href="" class="btn btn-danger btn-sm mt-2" style="position: relative !important; z-index: 1000;">Read More</a>
                                <i class="icofont-clip-board bg-icon text-mdnet text-right"></i>
                            </div>
                        </div>
                        <div data-aos="fade-up" class="col-md-6 mt-3 col-lg-4 d-flex align-items-stretch mb-5 mb-lg-0">
                            <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
                                <h4 class="title text-left"><a href="">Sparepart Management System</a></h4>
                                <p class="description">
                                    Medianet Technology have dedicated team
                                    with considerable experience of business 
                                    and technical operations in transaction 
                                    service,non profit organization, public sector
                                    and commercial organisations.
                                </p>
                                <a href="" class="btn btn-danger btn-sm mt-2" style="position: relative !important; z-index: 1000;">Read More</a>
                                <i class="icofont-presentation-alt bg-icon text-mdnet text-right"></i>
                            </div>
                        </div>
                    </div>
                <!-- </div>
            </div> -->
        </div>
    </section>
    <!-- End Services Section -->


    <!-- Section Help -->
    <section id="for-help" class="bg-forhelp mt-4">
        <div class="container" data-aos="fade-up">
            <div class="row">
                <div class="col-md-10 mt-2 col-lg-10 d-flex align-items-stretch mb-5 mb-lg-0">
                    <strong class="text-white text-bold"> 
                        <h3> We can help you with your project</h3>
                    </strong>
                </div>
                <div class="col-md-2 mt-2 col-lg-2 text-right">
                    <a href="<?= base_url("contactus") ?>" class="btn btn-primary">Contact Us</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End Section Help -->

</div>