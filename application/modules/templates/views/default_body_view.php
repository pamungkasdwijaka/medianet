<body>
    <!-- ======= Top Bar ======= -->
    <div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
        <div class="container d-flex">
            <div class="contact-info mr-auto">
                <i class="icofont-location-pin"></i> Find Us
                <i class="icofont-envelope"></i> <a href="mailto:contact@medianet.co.id">contact@medianet.co.id</a>
            </div>
            <div class="social-links">
                <a href="#" class="twitter">
                    <img src="<?= base_url() ?>assets/img/en.png" alt="" style="width: 20px; height: 20px;">
                    <img src="<?= base_url() ?>assets/img/id.png"alt="" style="width: 20px; height: 20px;">
                </a>
            </div>
        </div>
    </div>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">

            <h1 class="logo mr-auto"><a href="<?= base_url() ?>"><img src="<?= base_url() ?>assets/img/logo.png" alt=""><span>.</span></a></h1>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li class="active"><a href="<?= base_url() ?>">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#general-service">Services</a></li>
                    <!-- <li><a href="#portfolio">Portofolio</a></li> -->
                    <li><a href="#team">Team</a></li>
                    <li><a href="#contact">Contact</a></li>

                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header>

    <!-- <main id="main"> -->
        <?= $body ?>
    <!-- </main> -->

</body>