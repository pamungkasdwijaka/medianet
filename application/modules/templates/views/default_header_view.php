<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo base_url() ?>assets/img/icon-logo.ico">
        <title><?php echo $title ?></title>

        <!-- CSS Files -->
        <link href="<?php echo base_url() ?>resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>resources/icofont/icofont.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>resources/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>resources/venobox/venobox.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>resources/aos/aos.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>resources/boxicons/css/boxicons.min.css" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="<?php echo base_url() ?>assets/css/style.css" rel="stylesheet">
        

    </head>