    <!-- ======= Footer ======= -->
    <footer id="footer" class="mt-5">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 mt-5 mr-5">
                        <img src="<?= base_url() ?>assets/img/logo.png" alt="" width="300" height="80">
                    </div>

                    <div class="col-lg-2 col-md-6 footer-links ml-2">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Portofolio</a></li>
                            <li><a href="#">Team</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-6 col-md-6 footer-links ml-5">
                        <h4>Contact Us</h4>
                        <ul>
                            <li>
                                Jl. Jupiter Selatan. No.2
                                Sekejati, Kec. Buahbatu 
                                Kota Bandung, Jawa Barat 40286
                            </li>
                            <li>
                                Phone : (022) 87528027
                            </li>
                        </ul>

                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.5171092310693!2d107.65882681438725!3d-6.948163069950882!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e821cc787f21%3A0x610e2c57916c543!2sMedianet%20Technology.%20PT!5e0!3m2!1sid!2sid!4v1620181904431!5m2!1sid!2sid" style="border:0; width: 100%; height: 250px;" allowfullscreen="" loading="lazy"></iframe>
                    </div>

                </div>
            </div>
        </div>

        <div class="container py-4">
            <div class="copyright">
                &copy; Copyright <strong><span>PT. MEDIANET TECHNOLOGY</span></strong>. All Rights Reserved
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="<?php echo base_url() ?>resources/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>resources/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url() ?>resources/jquery.easing/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>resources/php-email-form/validate.js"></script>
    <script src="<?php echo base_url() ?>resources/waypoints/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url() ?>resources/counterup/counterup.min.js"></script>
    <script src="<?php echo base_url() ?>resources/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?php echo base_url() ?>resources/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="<?php echo base_url() ?>resources/venobox/venobox.min.js"></script>
    <script src="<?php echo base_url() ?>resources/aos/aos.js"></script>

    <!-- Template Main JS File -->
    <script src="<?php echo base_url() ?>assets/js/main.js"></script>
</html>