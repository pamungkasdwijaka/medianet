<div class="container-fluid p-0">
    
    <div id="detail-service" style="padding-top: 100px;" class="clearfix">
        <div class="bg-info" data-aos="fade-up" style="height: 450px;">
        	<div class="container">
				<div class="row">
					<div class="col-lg-5 mt-5">
						<h2 class="text-white">
							<?= $titleService ?>
						</h2>
						<p class="text-white" style="font-size: 20px">
							 Medianet Technology have dedicated team with considerable experience of business and technical operations in transaction service,non profit organization, public sector and commercial organisations.
						</p>
					</div>
					<div class="col-lg-7 mt-5">
						<img src="<?php echo base_url("assets/img/feature-TR-auto-reports.png") ?>" class="img-fluid">
					</div>
				</div>
        	</div>
		</div>
	</div>

	<div id="howit" style="padding: 10px 0;" class="clearfix">
		<div class="container mt-5 bg-white p-5">
            <h3 class="text-left text-black">HOW IT WORKS</h3>
            <div class="row mt-3">
            	<div class="col-lg-12">
            		<p>
	            		An automated report is a management tool used by professionals to create and share business reports at a specific time interval without the need to update the information each time. These updates are usually made in real-time with the help of automated reporting tools.
	            	</p>

	            	<p>
	            		Automated report scheduling and delivery takes information proactively to the right users at the right time. Report scheduling helps streamline information delivery as well as can help you optimize the running of many reports on your network, for example, during off-peak hours.
	            	</p>
            	</div>
            </div>
        </div>
	</div>

	<div id="howit-image" style="padding: 10px 0;" class="clearfix">
		<div class="container mt-5 bg-white p-5">
            <div class="row mt-3">
            	<div class="col-lg-12 text-center">
            		<img src="<?= base_url("assets/img/howit/howit.png") ?>" class="img-fluid">
            	</div>
            </div>
        </div>
	</div>

</div>