<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class ContentDB extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getContent($type)
    {
        $this->db->select("*")->from("content");

    	$this->db->where("menu", $type);

    	$query = $this->db->get()->result_array();

        return $query;
    }
}
