<div class="container-fluid" id="container-wrapper" ng-controller="about">

    <!-- Section About  -->
    <div class="row">
        <div class="col-lg-12 mb-4">
          <!-- Simple Tables -->
          <div class="card">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-dark pull-left">About Section</h6>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <span ng-if="edit == false" ng-bind-html="itemsAbout.description_en | toHtml">
                        </span>
                        <form ng-if="edit == true">
                            <input type="hidden" id="idb" ng-model="banner.idb">
                            <div class="form-group row" style="margin-bottom: 0px !important;">
                                <ul class="nav nav-pills mb-3 ml-1" id="pills-tab" role="tablist">
                                  <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="pills-english-tab" data-toggle="pill" href="#pills-english" role="tab" aria-controls="pills-english" aria-selected="true">English</a>
                                  </li>
                                  <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="pills-indonesia-tab" data-toggle="pill" href="#pills-indonesia" role="tab" aria-controls="pills-indonesia" aria-selected="false">Indonesia</a>
                                  </li>
                                </ul>
                            </div>

                             <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-english" role="tabpanel" aria-labelledby="pills-english-tab">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <textarea class="form-control" id="desc" name="desc" ng-model="itemsAbout.description_en" ui-tinymce="tinymceOptions">{{itemsAbout.description_en}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-indonesia" role="tabpanel" aria-labelledby="pills-indonesia-tab">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <textarea class="form-control" id="desc" name="desc" ng-model="itemsAbout.description_id" ui-tinymce="tinymceOptions">{{itemsAbout.description_id}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-6">
                        <img src="<?= base_url("assets/img/about/rawpixel-579231-unsplash.jpg") ?>" ng-if="editImage == false" class="img-fluid">
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <div class="row">
                    <div class="col-sm-6">

                        <a ng-if="edit == false" class="mt-1 btn btn-primary pull-left" href="" ng-click="editText()"><i class="fas fa-pencil-alt"></i>  Edit</a>
                        <a ng-if="edit == true" ng-click="cancelEdit()" class="mt-1 btn btn-danger pull-left" href="" ng-click="editText()"><i class="fas fa-cancel"></i>  Cancel</a>
                        <a ng-if="edit == true" class="mt-1 btn btn-success pull-left" href="" ng-click="editText()"><i class="fas fa-check"></i>  Save</a>

                    </div>

                    <div class="col-sm-6">

                        <a ng-if="editImage == false" class="mt-1 btn btn-primary pull-left" href="" ng-click="frmImage()"><i class="fas fa-pencil-alt"></i>  Change Image</a>
                        
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <!--Section About-->

    <!-- Modal Add Banner -->
    <div class="modal fade" id="mdlBanner" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Add New Banner</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                <input type="hidden" id="idb" ng-model="banner.idb">
                <div class="form-group row" style="margin-bottom: 0px !important;">
                    <ul class="nav nav-pills mb-3 ml-1" id="pills-tab" role="tablist">
                      <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="pills-english-tab" data-toggle="pill" href="#pills-english" role="tab" aria-controls="pills-english" aria-selected="true">English</a>
                      </li>
                      <li class="nav-item" role="presentation">
                        <a class="nav-link" id="pills-indonesia-tab" data-toggle="pill" href="#pills-indonesia" role="tab" aria-controls="pills-indonesia" aria-selected="false">Indonesia</a>
                      </li>
                    </ul>
                </div>

                 <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-english" role="tabpanel" aria-labelledby="pills-english-tab">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="title" ng-model="banner.title.en" placeholder="Title">
                            </div>
                            <div class="col-sm-12 mt-2">
                                <input type="text" class="form-control" id="subtitle" ng-model="banner.subtitle.en" placeholder="Sub Title">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-indonesia" role="tabpanel" aria-labelledby="pills-indonesia-tab">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="title" ng-model="banner.title.id" placeholder="Title">
                            </div>
                            <div class="col-sm-12 mt-2">
                                <input type="text" class="form-control" id="subtitle" ng-model="banner.subtitle.id" placeholder="Sub Title">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="title" ng-model="banner.title">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="subtitle" class="col-sm-2 col-form-label">Subtitle</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="subtitle" ng-model="banner.subtitle">
                    </div>
                </div> -->

               <div class="form-group row">
                    <label for="photo" class="col-2">Image</label>
                    <div class="col-sm-10 input-file input-file-msg input-file-sm" id="file-banner">
                        <input type="file" id="iPhoto-banner" name="photo" file-model="photos.photoBanner" class="form-control form-control-file inputFile inputFile-img" accept="image/*">
                        <label for="iPhoto-banner" class="m-0"><i class="dripicons dripicons-plus"></i></label>
                        <div class="img-preview"></div>
                    </div>
                    <div class="invalid-feedback"></div>
                </div>

                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="status" required ng-model="banner.status">
                           <option selected value="active">Active</option>
                           <option value="nonactive">Non Active</option>
                        </select>
                    </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary btn-block m-0 btn-save" ng-click="saveBanner()">Save</button>
          </div>
        </div>
      </div>
    </div>
</div>
