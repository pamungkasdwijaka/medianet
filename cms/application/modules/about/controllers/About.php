<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class About extends Cmnet_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model(["ContentDB"]);
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Section About';
		$this->data['version'] = $this->uri->segment(1);
		

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/js/app/about.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
		);

		$this->template->load($this->data, null, 'index');

	}

	public function content_get()
	{
		$type	= "about";
		$sql	= $this->ContentDB->getContent($type);

		$return = [
			"data"	=> $sql[0],
			"msg"	=> "Success"
		];

		$this->response($return);
	}
}
