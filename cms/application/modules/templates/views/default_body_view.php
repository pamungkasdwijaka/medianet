<style>
@keyframes loader-animation {
  0% {
      width: 0%;
  }
  49% {
      width: 100%;
      left: 0%
  }
  50% {
      left: 100%;
  }
  100% {
      left: 0%;
      width: 100%
  }
}
.loader {
  height: 3px;
  width: 100%;
}
.loader .btn-danger {
  height: 3px;
  border-radius: 0px 0px 6px 6px;
  background-color: red;
  animation-name: loader-animation;
  animation-duration: 3s;
}
.loader .btn-success {
  height: 3px;
  border-radius: 0px 0px 6px 6px;
  background-color: #15d4be;
  animation-name: loader-animation;
  animation-duration: 3s;
}
</style>
<body id="page-top" ng-app="medianet">
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url() ?>">
        <div class="sidebar-brand-icon">
          <img src="<?= base_url() ?>assets/img/logo.png" class="img-fluid">
        </div>
        <!-- <div class="sidebar-brand-text mx-3">
MNET
</div> -->
      </a>
      <hr class="sidebar-divider my-0">
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBootstrap"
          aria-expanded="true" aria-controls="collapseBootstrap">
          <i class="far fa-fw fa-window-maximize"></i>
          <span>Homepage Section</span>
        </a>
        <div id="collapseBootstrap" class="collapse mt-2" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
          <div class="bg-info py-2 collapse-inner rounded">
            <a class="collapse-item text-white" href="<?= base_url() ?>">Section Banner</a>
            <a class="collapse-item text-white" href="<?= base_url("about") ?>">Section About</a>
          </div>
        </div>
      </li>
      <hr class="sidebar-divider">
      <div class="version" id="version-ruangadmin">
      </div>
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <!-- TopBar -->
        <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
          <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
            <i class="fa fa-bars">
            </i>
          </button>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown"
                 aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw">
                </i>
              </a>
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                   aria-labelledby="searchDropdown">
                <form class="navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-1 small" placeholder="What do you want to look for?"
                           aria-label="Search" aria-describedby="basic-addon2" style="border-color: #03A9F4;">
                    <div class="input-group-append">
                      <button class="btn btn-info" type="button">
                        <i class="fas fa-search fa-sm">
                        </i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                 aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="assets/img/user.jpg" style="max-width: 60px">
                <span class="ml-2 d-none d-lg-inline text-white small text-uppercase">
                  <?= $users["username"] ?>
                </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400">
                  </i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400">
                  </i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400">
                  </i>
                  Activity Log
                </a>
                <div class="dropdown-divider">
                </div>
                <a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400">
                  </i>
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </nav>
        <!-- Topbar -->
        <?= $body ?>
      </div>
    </div>
  </div>

  <div class="modal" style="background: rgba(122, 14, 18, 0.3) !important;" id="popup-msg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content mt-5" style="background: transparent !important; color: #fff !important; border: none !important;">
            <div class="modal-body" style="text-align: center;">

            </div>
            <div class="loader">
              <button class="btn-danger" style="display: none;"></button>
              <button class="btn-success" style="display: none;"></button>
            </div>
          </div>
      </div>
  </div> 
</body>
