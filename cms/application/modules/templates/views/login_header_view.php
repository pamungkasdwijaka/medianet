<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>CMS Medianet Technology</title>

	<!-- Bootstrap Core Css -->
	<link href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Waves Effect Css -->
	<link href="<?= base_url() ?>assets/plugins/node-waves/waves.css" rel="stylesheet" />

	<!-- Animation Css -->
	<link href="<?= base_url() ?>assets/plugins/animate-css/animate.css" rel="stylesheet" />

	<!-- Custom Css -->
	<link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

	<!-- AngularJS -->
	<script src="<?php echo base_url() ?>assets/plugins/angularjs/angular.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/js/app.js"></script>
	<script src="<?php echo base_url() ?>assets/plugins/angularjs/dirPagination.js"></script>

	<?php
        if(isset($js) && count($js) > 0){
            foreach ($js as $key => $vjs) {
                echo '<script type="text/javascript" src="'.(is_int($key) ? base_url($vjs) : $vjs).'"></script>';
            }
        }

        if(isset($css) && count($css) > 0){
            foreach ($css as $vcss) {
                echo '<link rel="stylesheet" type="text/css" href="'.base_url($vcss).'">';
            }
        }
    ?>

</head>