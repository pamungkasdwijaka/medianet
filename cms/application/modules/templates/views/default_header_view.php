<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?= $title ?></title>

	<link rel="icon" href="<?= base_url() ?>assets/img/icon-logo.ico" type="image/x-icon">

	<link href="<?= base_url() ?>assets/plugins/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!-- <link href="<?= base_url() ?>assets/css/medianet-style.min.css" rel="stylesheet"> -->
	<link href="<?= base_url() ?>assets/css/medianet-style.css" rel="stylesheet">

	<!-- AngularJS -->
	<script src="<?php echo base_url() ?>assets/plugins/angularjs/angular.min.js" type="text/javascript"></script>
	<script src="<?= base_url("assets/js/app.js?").date("Ymd His") ?>"></script>
	<script src="<?php echo base_url() ?>assets/plugins/angularjs/dirPagination.js"></script>

	<?php
        if(isset($js) && count($js) > 0){
            foreach ($js as $key => $vjs) {
                echo '<script type="text/javascript" src="'.(is_int($key) ? base_url($vjs) : $vjs).'"></script>';
            }
        }

        if(isset($css) && count($css) > 0){
            foreach ($css as $vcss) {
                echo '<link rel="stylesheet" type="text/css" href="'.base_url($vcss).'">';
            }
        }
    ?>
</head>