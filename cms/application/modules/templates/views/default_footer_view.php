	<!-- Scroll to top -->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>
	<script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/jquery-easing/jquery.easing.min.js"></script>
	<script src="<?= base_url() ?>assets/js/medianet.min.js"></script>

    <script type="text/javascript" src="<?= base_url('assets/plugins/tinymce/tinymce.js?').date('Ymd-His') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/plugins/angular-ui-tinymce/src/tinymce.js?').date('Ymd-His') ?>"></script>
</html>