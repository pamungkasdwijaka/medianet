<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dashboard extends Cmnet_Controller {

	
	function __construct()
	{
		parent::__construct();
		$this->load->model(["BannerDB"]);
	}

	
	public function index_get()
	{
	    // Initialize the array with a 'title' element for use for the <title> tag.
		$this->data['title'] = 'Homepage - Section Banner';
		$this->data['version'] = $this->uri->segment(1);
		

		$this->data['js'] = array(
			// 'assets/bootstrap/datepicker/js/bootstrap-datetimepicker.min.js',
			'assets/js/app/homepage.js'
		);

		$this->data['css'] = array(
			// 'assets/bootstrap/datepicker/css/bootstrap-datetimepicker.min.css'
		);

		$this->template->load($this->data, null, 'index');

	}

	public function banner_get()
	{
		$page  		= $this->get('page') ? $this->get('page') : 1;
        $take  		= $this->get('take') ? $this->get('take') : 20;
        $skip 		= ($page - 1) == 0 ? 0 : ($page - 1) * $take;
        $keyword 	= $this->get('keyword');

		$sql = $this->BannerDB->getBanner($page, $take, $skip, $keyword);

		$count = $this->db->count_all('banner');

		if (empty($sql)) {

			$return = [
				"data"	=> null,
				"msg"	=> "Data Empty !",
				"count" => 0
			];

		} else {

			$return = [
				"data"	=> $sql,
				"msg"	=> "Success",
				"count" => $count
			];

		}

		$this->response($return);
	}

	public function banner_post()
	{
		// print_r("<pre>");
		$title 		= json_decode($this->post("title"));
		$subtitle 	= json_decode($this->post("subtitle"));

		$data = [
			"title_en" 		=> $title->en,
			"title_id" 		=> $title->id,
			"subtitle_en" 	=> $subtitle->en,
			"subtitle_id" 	=> $subtitle->id,
			"status"		=> $this->post("status")
		];

		if (!empty($this->post('idb'))) {

            $this->db->where('id', $this->post('idb'))->update("banner", $data);
            $id = $this->post('idb');

        } else{
            $this->db->insert("banner", $data);
            $id = $this->db->insert_id();
        }

        // print_r($id);
        // exit();

		if ($id) {
			if (!empty($_FILES)) {

				$path	= realpath(APPPATH . '../assets/img/banner');
				$temp = explode(".", $_FILES["photo"]["name"]);
	            $filename = "banner_".$id.substr(md5(time()), 0, 10) . '.' . end($temp);

	            if (!file_exists($path) && !is_dir($path)) {
	                    mkdir($path, 0777, true);
	                }

	            if(move_uploaded_file($_FILES['photo']['tmp_name'], $path . '/' . $filename))
	                $this->db->where('id', $id)->update("banner", ['img' => $filename]);
				
			}

			$icon = "success";
			$error = false;
            $message = !empty($this->post('idb')) ? "Banner successfully updated!" : "Banner successfully created!";
		} else {
			$error = true;
			$icon = "error";
            $message = !empty($this->post('idb')) ? "Banner failed update, please try again!" : "Banner failed created, please try again!";
		}

		$this->response([
			'icon'		=> $icon,
            'error' 	=> $error,
            'message'   => $message
        ]);
	}
}
