<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class BannerDB extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getBanner($page, $take, $skip, $keyword)
    {
    	// print_r("<pre>");
    	// print_r($page);
    	// print_r("<br>");
    	// print_r($take);
    	// print_r("<br>");
    	// print_r($skip);
    	// print_r("<br>");
    	// print_r($keyword);
    	// exit();

        $this->db->select("*")->from("banner");

    	$this->db->limit($take, $skip);

    	$query = $this->db->get()->result_array();

        return $query;
    }
}
