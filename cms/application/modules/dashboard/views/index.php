<div class="container-fluid" id="container-wrapper" ng-controller="homepage">

    <!-- Section Banner  -->
    <div class="row">
        <div class="col-lg-12 mb-4">
          <!-- Simple Tables -->
          <div class="card">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-dark pull-left">Banner Section</h6>
              <a class="btn btn-primary pull-right" href="" ng-click="addBanner()"><i class="fas fa-plus-square"></i>  Add Banner</a>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush table-hover">
                <thead class="thead-info">
                  <tr>
                    <th rowspan="2" class="text-center">#</th>
                    <th colspan="2" class="text-center">Title</th>
                    <th colspan="2" class="text-center">Subtitle</th>
                    <th rowspan="2" class="text-center">Images</th>
                    <th rowspan="2" class="text-center">Status</th>
                    <th rowspan="2" class="text-center">Action</th>
                  </tr>
                  <tr>
                      <th class="text-center" style="background: none;"><img src="<?= base_url() ?>assets/img/en.png" alt="" style="width: 20px; height: 20px;"></th>
                      <th class="text-center" style="background: none;"><img src="<?= base_url() ?>assets/img/id.png" alt="" style="width: 20px; height: 20px;"></th>
                      <th class="text-center" style="background: none;"><img src="<?= base_url() ?>assets/img/en.png" alt="" style="width: 20px; height: 20px;"></th>
                      <th class="text-center" style="background: none;"><img src="<?= base_url() ?>assets/img/id.png" alt="" style="width: 20px; height: 20px;"></th>
                  </tr>
                </thead>
                <tbody>
                  <tr dir-paginate="(key, value) in itemsBanner|itemsPerPage:itemsPerPageBanner" total-items="totalitemsBanner" current-page="currentPageBanner" pagination-id="pageBanner">
                    <td ng-bind="(key+no)"></td>
                    <td ng-bind="value.title_en"></td>
                    <td ng-bind="value.title_id"></td>
                    <td ng-bind="value.subtitle_en"></td>
                    <td ng-bind="value.subtitle_id"></td>
                    <td class="text-center">
                      <img ng-src="{{'assets/img/banner/'+value.img}}" class="img-fluid img-banner-mnet">
                    </td>
                    <td class="text-center">
                      <span ng-if="value.status == 'active'" class="badge badge-success">Active</span>
                      <span ng-if="value.status == 'nonactive'" class="badge badge-danger">Non Active</span>
                    </td>
                    <td class="text-center">
                      <a href="#" class="btn btn-sm btn-primary mb-1">
                        <i class="fas fa-pencil-alt"></i>
                      </a>
                      <a href="#" ng-if="value.status == 'nonactive'" class="btn btn-sm btn-success">
                        <i class="fas fa-check"></i>
                      </a>
                      <a href="#" ng-if="value.status == 'active'" class="btn btn-sm btn-danger">
                        <i class="fas fa-times-circle"></i>
                      </a>
                    </td>
                  </tr>
                  <tr ng-if="pendingBanner">
                    <td colspan="10" align="center">
                      <small>Loading</small> <i class="fas fa-spinner fa-pulse"></i>
                    </td>
                  </tr>
                  <tr ng-if="!pendingBanner && totalitemsBanner === 0">
                    <td colspan="10" align="center">
                      <i ng-bind="'Sorry data not found'"></i>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="card-footer">
              <dir-pagination-controls style="float: right;"
                    template-url="<?= base_url('assets/plugins/angularjs') ?>/dirPagination.tpl.html"
                    direction-links="false" pagination-id="pageBanner" boundary-links="true"
                    on-page-change="getBanner(newPageNumber)"></dir-pagination-controls>
            </div>
          </div>
        </div>
    </div>
    <!--Section Banner-->

    <!-- Modal Add Banner -->
    <div class="modal fade" id="mdlBanner" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="staticBackdropLabel">Add New Banner</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                <input type="hidden" id="idb" ng-model="banner.idb">
                <div class="form-group row" style="margin-bottom: 0px !important;">
                    <ul class="nav nav-pills mb-3 ml-1" id="pills-tab" role="tablist">
                      <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="pills-english-tab" data-toggle="pill" href="#pills-english" role="tab" aria-controls="pills-english" aria-selected="true">English</a>
                      </li>
                      <li class="nav-item" role="presentation">
                        <a class="nav-link" id="pills-indonesia-tab" data-toggle="pill" href="#pills-indonesia" role="tab" aria-controls="pills-indonesia" aria-selected="false">Indonesia</a>
                      </li>
                    </ul>
                </div>

                 <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-english" role="tabpanel" aria-labelledby="pills-english-tab">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="title" ng-model="banner.title.en" placeholder="Title">
                            </div>
                            <div class="col-sm-12 mt-2">
                                <input type="text" class="form-control" id="subtitle" ng-model="banner.subtitle.en" placeholder="Sub Title">
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-indonesia" role="tabpanel" aria-labelledby="pills-indonesia-tab">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="title" ng-model="banner.title.id" placeholder="Title">
                            </div>
                            <div class="col-sm-12 mt-2">
                                <input type="text" class="form-control" id="subtitle" ng-model="banner.subtitle.id" placeholder="Sub Title">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="title" ng-model="banner.title">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="subtitle" class="col-sm-2 col-form-label">Subtitle</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="subtitle" ng-model="banner.subtitle">
                    </div>
                </div> -->

               <div class="form-group row">
                    <label for="photo" class="col-2">Image</label>
                    <div class="col-sm-10 input-file input-file-msg input-file-sm" id="file-banner">
                        <input type="file" id="iPhoto-banner" name="photo" file-model="photos.photoBanner" class="form-control form-control-file inputFile inputFile-img" accept="image/*">
                        <label for="iPhoto-banner" class="m-0"><i class="dripicons dripicons-plus"></i></label>
                        <div class="img-preview"></div>
                    </div>
                    <div class="invalid-feedback"></div>
                </div>

                <div class="form-group row">
                    <label for="status" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <select class="custom-select" id="status" required ng-model="banner.status">
                           <option selected value="active">Active</option>
                           <option value="nonactive">Non Active</option>
                        </select>
                    </div>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary btn-block m-0 btn-save" ng-click="saveBanner()">Save</button>
          </div>
        </div>
      </div>
    </div>
</div>
