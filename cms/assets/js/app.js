const base_url = (location.hostname === "localhost" || location.hostname === "127.0.0.1") ? window.location.origin + "/medianet/cms/" : window.location.origin + "/";
var mainUrl = window.location.origin+'/medianet/cms/';
var originUrl = window.location.origin+'/medianet/';

var mainApp = angular.module('medianet', ['angularUtils.directives.dirPagination', 'ui.tinymce'])

.directive('fileModel', ['$parse', 'appHelper', function ($parse, appHelper) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          
          element.bind('change', function(){

            if(element[0].files[0].size > 512000){
                appHelper.showMessage('error', 'Oopss file size must bellow 500kb!');
                
                $('#'+element[0].id).val('');

                $('#'+element[0].parentElement.id).addClass('is-invalid-img');
                $('#'+element[0].parentElement.id).next('.invalid-feedback').html('Please choose again!').show();
            }
            else if (element[0].files[0].type != 'image/jpeg' && element[0].files[0].type != 'image/png') {
                appHelper.showMessage('error', 'Oopss wrong extension file!<br>Please use file jpg/png!');
                
                $('#'+element[0].id).val('');

                $('#'+element[0].parentElement.id).addClass('is-invalid-img');
                $('#'+element[0].parentElement.id).next('.invalid-feedback').html('Please choose again!').show();
            }else{
                console.log(element[0].parentElement.id);
                $('#'+element[0].parentElement.id).removeClass('is-invalid-img');
                $('#'+element[0].parentElement.id).next('.invalid-feedback').hide();
                
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            }
          });
       }
    };
 }])

.directive('fileUpload', ['$parse', 'appHelper', '$http', function ($parse, appHelper, $http) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          
          element.bind('change', function(){

            $('.list-loading').empty();
            
            var fileSize = element[0].files[0].size/1024/1024;
            var fileType = ['image/png','image/jpeg','application/pdf','application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document'];

            if(fileSize > 5){
                appHelper.showMessage('error', 'Oopss file size must bellow 5MB!');
                
                $('#'+element[0].id).val('');

                $('#'+element[0].parentElement.id).addClass('is-invalid-img');
                $('#'+element[0].parentElement.id).next('.invalid-feedback').html('Please choose again!').show();
            }
            else if (fileType.indexOf(element[0].files[0].type) === -1) {
                appHelper.showMessage('error', 'Oopss invalid extension file!');
                
                $('#'+element[0].id).val('');

                $('#'+element[0].parentElement.id).addClass('is-invalid-img');
                $('#'+element[0].parentElement.id).next('.invalid-feedback').html('Please choose again!').show();
            }else{

                $('#'+element[0].parentElement.id).removeClass('is-invalid-img');
                $('#'+element[0].parentElement.id).next('.invalid-feedback').hide();

                $('.list-loading').html('<li class="mb-2"><span class="badge badge-pill badge-primary badge-loading font-weight-normal"><i class="dripicons dripicons-loading"></i> uploading</span></li>');
                
                var formData = new FormData();
                formData.append("path", 'catalog');
                formData.append("isName", true);
                formData.append("photo", element[0].files[0]);
                
                //get the current value as evaluated against the $scope
                var ar = $parse(attrs.fileUpload)(scope);

                //if the value isn't an array then make it one
                if (!Array.isArray(ar)) {
                    ar = [];
                    $parse(attrs.fileUpload).assign(scope, ar);
                }

                $http({
                    url: mainUrl+'solution/product/upload',
                    data: formData,
                    method: 'POST',
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(
                    function successCallbacks(response){

                        if (response.data.error) {
                            $('.list-loading').html('<li class="mb-2"><span class="badge badge-pill badge-primary badge-loading font-weight-normal">'+response.data.message+'</span></li>');
                        }else{
                            ar.push(response.data.data);
                            $('.list-loading').empty();
                        }

                        $('#'+element[0].id).val('');

                    },
                    function errorCallback(response) {}
                );

            }

          });
       }
    };
 }])

.service('httpHandler', ['$http', '$q', '$injector', function($http, $q, $injector){

    this.send = function (opt, loading) {

        var canceler = $q.defer();
        var appHelper = $injector.get('appHelper');
        
        // Set options
        var options = {
            headers: {},
            method: 'GET',
            data: {},
            params: {},
            timeout: canceler.promise,
        };

        $.extend(options, opt);

        // if(options.method == 'POST'){
        //     options.headers['Content-Type'] = 'application/x-www-form-urlencoded';
        //     options.data = options.data;
        // }

        var request = $http(options);

        request.then(
            function successCallbacks(response){

            }, 
            function errorCallback(response, statusText) {
                
                if(response.status == 401)
                {
                    appHelper.showMessage('error','Unauthorized, please contact admin!');                    
                }else if(response.status == 400){
                    appHelper.showMessage('error','Oops bad request, please contact admin!');
                }else if(response.status == 403){
                    appHelper.showMessage('error','Oops you dont have access!');
                }else if(response.status == 404){
                    appHelper.showMessage('error','Oops page not found!');
                }else if(response.status == 405){
                    appHelper.showMessage('error','Oops method not allowed, please contact admin!');
                }else if(response.status == 408){
                    appHelper.showMessage('error','Oops request timeout, please reload again!');
                }else if(response.status == 500){
                    appHelper.showMessage('error','Oops internal server error, please reload again!');
                }else if(response.status == 502){
                    appHelper.showMessage('error','Oops bad gateway, please reload again!');
                }else if(response.status == 503){
                    appHelper.showMessage('error','Oops service unavailable, please reload again!');
                }

            });
        
        return request;
    }

}])

.service('appHelper', ['httpHandler', function(httpHandler){
    
    this.showMessage = function (e, msg) {
        
        if(e == 'success'){
            err = '<h1><i class="dripicons dripicons-checkmark text-success"></i></h1>';
        }else if(e == 'error'){
            err = '<h1><i class="dripicons dripicons-warning text-danger"></i></h1>';
        }else{
            err = e;
        }

        $('#popup-msg').find('.modal-body').html(err+'<p>'+msg+'</p>');
        $('#popup-msg').modal('show');
    }

}])

.filter('dateToISO', function() {
    return function(date) {
        const parsed = Date.parse(date);
        if (!isNaN(parsed)) {
            return parsed;
        }

        if(date != undefined)
            return Date.parse(date.replace(/-/g, '/').replace(/[a-z]+/gi, ' '));
    };
})

.filter('decodeBase64', function() {
    return function(text) {
        return atob(text);
    }
})

.filter('toJson', function() {
    return function(jsonStr, column) {
        return JSON.parse(jsonStr)[column];
    };
})

.filter('toHtml', ['$sce', function($sce) {
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}])

.filter('setJson', ['$sce', function($sce) {
    return function(text, lang) {
        var txt;

        if (typeof text == 'object') {
            txt = text[lang];
        }else if(IsJsonString(text)){
            txt = JSON.parse(text)[lang];
        }else
            txt = text;

        return $sce.trustAsHtml(txt);
    };
}])

.filter('deCode', ['$sce', function($sce) {
    return function(text, lang) {
        var txt;

        if(IsJsonString(atob(text))){
            txt = JSON.parse(b64DecodeUnicode(text))[lang];
        }else
            txt = b64DecodeUnicode(text);


        return $sce.trustAsHtml(txt);
    };
}]);

function IsJsonString(str) {
    try {
        var json = JSON.parse(str);
        return true;
    } catch (e) {
        return false;
    }
}

function b64DecodeUnicode(str) {
    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
}

function b64EncodeUnicode(str) {
    return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode(parseInt(p1, 16))
    }))
}