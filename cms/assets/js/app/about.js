mainApp

.controller('about', ['$scope', 'httpHandler', 'appHelper', '$attrs', '$timeout', '$filter', function ($scope, httpHandler, appHelper, $attrs, $timeout, $filter) {

	$scope.edit = false;
    $scope.editImage = false;

	$scope.tinymceOptions = {
        relative_urls: false,
        remove_script_host : false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor filemanager code"
        ],
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | code image media link',
        content_css: base_url+'assets/plugins/bootstrap/css/bootstrap.css?'+ new Date().getTime()+', '+base_url+'assets/css/medianet-style.css?'+ new Date().getTime(),
        height: 300,
        filemanager_title: 'File Manager',
        filemanager_access_key: "CHrfEWLWhBVgAkKqu6OY",
        external_filemanager_path:"../assets/plugins/tinymce/plugins/filemanager/",
        external_plugins: { "filemanager" : "./plugins/filemanager/plugin.min.js"}
    }

    $scope.getContent = function () {
    	httpHandler.send({
            url: mainUrl+'about/content',
        }).then(
        function successCallbacks(response){
            
            $scope.itemsAbout = response.data.data;

        }, 
        function errorCallback(response) {
            appHelper.showMessage('error', 'Ooppss something wrong, please try again!');
        });
    }

    $scope.getContent();

    // Section About
    $scope.editText = function () {
    	$scope.edit = true;
    }

    $scope.cancelEdit = function () {
		$scope.edit = false;
    }

    $scope.frmImage = function () {
        $scope.editImage = false;
    } 
}]);