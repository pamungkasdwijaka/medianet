mainApp

.controller('homepage', ['$scope', 'httpHandler', 'appHelper', '$attrs', '$timeout', '$filter', function ($scope, httpHandler, appHelper, $attrs, $timeout, $filter) {

	$scope.edit = false;

	$scope.tinymceOptions = {
        relative_urls: false,
        remove_script_host : false,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
            "table contextmenu directionality emoticons paste textcolor filemanager code"
        ],
        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright alignjustify | code image media link',
        content_css: base_url+'assets/plugins/bootstrap/css/bootstrap.css?'+ new Date().getTime()+', '+base_url+'assets/css/style.min.css?'+ new Date().getTime(),
        height: 300,
        filemanager_title: 'File Manager',
        filemanager_access_key: "CHrfEWLWhBVgAkKqu6OY",
        external_filemanager_path:"../assets/plugins/tinymce/plugins/filemanager/",
        external_plugins: { "filemanager" : "./plugins/filemanager/plugin.min.js"}
    }


	//banner
    $scope.noBanner = 1;
    $scope.itemsBanner = {};
    $scope.itemsPerPageBanner = 5;
    $scope.orderBanner = null;
    $scope.keywordBanner = {};

    $scope.getBanner = function(pageno){
    	console.log(mainUrl);
        $scope.pendingBanner = true;
        $scope.totalitemsBanner = 0;
        $scope.itemsBanner = {};
        
        if(pageno == 1)
            $scope.no = 1;
        else
            $scope.no = (pageno*$scope.itemsPerPageBanner) - ($scope.itemsPerPageBanner - 1);

        httpHandler.send({
            url: mainUrl+'dashboard/banner',
            params: {
                page: pageno,
                take: $scope.itemsPerPageBanner,
                order: $scope.orderBanner,
                keyword: $scope.keywordBanner
            }
        }).then(
            function successCallbacks(response){
                
                $scope.pendingBanner = false;

                if (response.data.count == 0)
                    $scope.currentPageBanner = 1;

                $scope.itemsBanner = response.data.data;
                $scope.totalitemsBanner = response.data.count;

            }, 
            function errorCallback(response) {
                // appHelper.showMessage('error', 'Ooppss something wrong, please try again!');
            });

    }

    $scope.getBanner(1);

    $scope.addBanner = function () {
    	$("#mdlBanner").modal("show");
    }

    // SAVE BANNER
    $scope.photos = {
        photoBanner: null,
    }
    $scope.saveBanner = function () {
    	console.log($scope.banner);
    	console.log($scope.photos.photoBanner);

    	$('.btn-save').html('<i class="fas fa-spinner fa-pulse"></i>').attr('disabled');

        var formData = new FormData();
        $.each($scope.banner, function(index, val) {
            formData.append(index, (typeof val === 'object' ? JSON.stringify(val) : val));
        });

        formData.append("photo", $scope.photos.photoBanner);

        httpHandler.send({
            url: mainUrl+'dashboard/banner',
            data: formData,
            method: 'POST',
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(
        function successCallbacks(response){

			$timeout( function(){
	            window.location.href = mainUrl+'dashboard';
	        }, 2000);

            //time
            $scope.time = 0;
	        $timeout(function(){ $('#mdlBanner').modal('hide');
	            if (response.data.icon == 'success') {
	           		$('#popup-msg').find('.btn-success').removeAttr('style');
	            } else {
	            	$('#popup-msg').find('.btn-danger').removeAttr('style');
	            }
	        },300);
                    
            // timer callback
            var timer = function() {
                if( $scope.time > 0 ) {

		            $scope.time += 1000;
					$timeout(timer, 1000);
                }
            }
                    
            //run!!
            $timeout(function(){
            	appHelper.showMessage("success", response.data.msg);
            },500);
            $timeout(timer, 1000);
        }, 
        function errorCallback(response) {
            // appHelper.showMessage('error', 'Ooppss something wrong, please try again!');
        });
    }
}]);